const fs = require('fs');

const crearArchivo = (base = 6) => {
  try {
    let salida = '';

    for(let i = 1; i <= 100; i++){
      salida += `${base} + ${i} = ${base + i}\n`;
    }

    fs.writeFileSync(`salida-operaciones/tabla de suma ${base}.txt`, salida);
    console.log(`tabla de suma ${base}.txt creada`.cyan); ;
  }
  catch (error) {
    throw error;
  }
};
//Exportamos el archivo
module.exports = {
  generarArchivoS: crearArchivo
};

