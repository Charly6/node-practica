const fs = require('fs');

const crearArchivo = (base = 6) => {
  try {
    let salida = '';

    for(let i = 1; i <= 100; i++){
      salida += `${base} x ${i} = ${base * i}\n`;
    }

    fs.writeFileSync(`salida-operaciones/tabla de multiplica ${base}.txt`, salida);
    console.log(`tabla de multiplicar ${base}.txt creada`.cyan); ;
  }
  catch (error) {
    throw error;
  }
};
//Exportamos el archivo
module.exports = {
  generarArchivoM: crearArchivo
};

