const { generarArchivoM } = require('./helpers/multiplicar');
const { generarArchivoD } = require('./helpers/dividir');
const { generarArchivoR } = require('./helpers/restar');
const { generarArchivoS } = require('./helpers/sumar');

//const readline = require('readline-sync'); 
const argv = require('yargs').argv; 
require('colors')

console.log('base: yargs'.green, argv.base);

//var path = readline.question("presione enter para continuar...");

generarArchivoM(argv.base)
generarArchivoD(argv.base)
generarArchivoR(argv.base)
generarArchivoS(argv.base)