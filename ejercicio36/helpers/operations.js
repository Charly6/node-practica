const fs = require('fs');

const generarList = () =>{
  list = []
  for (let i = 0; i < 500000; i++) {
    list.push(random())
  }
  return list
}

const random = () => {
  return Math.floor((Math.random() * (700000 - 100 + 1)) + 100);
}

const steps = (n) => {
  const numReal = n
  listaValores = []
  let counter = 0;
  if (n > 0 && Number.isInteger(n)) {
    while (n !== 1) {
      if (n % 2 === 0) {                                      
        n = n / 2; 
        counter++;
      } else if (n % 2 !== 0) {
        n = (n * 3) + 1;
        counter++;
      }
      listaValores.push(n)
    }
    return `Valor ${numReal}: ${listaValores}\n`;
  } else {
      throw new Error(`Valor no valido ${n}`);
  }
}

const saveFile = (list) => fs.writeFileSync(`salida-collatz/resultado.txt`, list);

//Exportamos el archivo
module.exports = {
  generar: generarList,
  step: steps,
  save: saveFile
};
