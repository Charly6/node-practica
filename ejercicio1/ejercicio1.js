const clientes = [
  {id: 1, nombre: "Leonor"},
  {id: 2, nombre: "Jacinto"},
  {id: 3, nombre: "Waldo"}
]
const pagos = [
  {id: 1, pago: 1000, moneda: "Bs"},
  {id: 2, pago: 1800, moneda: "Bs"}
]

// CALL BACK
const getPagosById = (id, callback) => {
  const pago = pagos.find(pago => pago.id === id)
  setTimeout(() => {
    console.log("CALL BACK PAGO");
    callback(pago);
  }, 1500);
};

const getClientById = (id, callback) => {
  const client = clientes.find(client => client.id === id)
  setTimeout(() => {
    console.log("CALL BACK CLIENTE");
    callback(client);
  }, 1500);
};


getClientById(1, (client) => {
  console.log(client);
});

getPagosById(1, (client) => {
  console.log(client);
});

//CALL BACK HELL

const getPagosH = (id, callback) => {
  const pago = pagos.find(pago => pago.id === id)
    callback(pago);
};

const getClientHell = (id, callback) => {
  const client = clientes.find(client => client.id === id)
  setTimeout(() => {
    console.log("CALL BACK HELL");
    callback(client);
    getPagosH(id, callback)
  }, 2500);
};

getClientHell(2, obj => console.log(obj))

//Promesa
const getCliente = (id) => {
  return new Promise((resolve, reject) => {
    const client = clientes.find(c => c.id === id);

    if(client){
      resolve(client);
    } else {
      reject(`No existe el cliente con el id ${id}`);
    }
  });
};

const getPagoP = (id) => {
  return new Promise((resolve, reject) => {
    const pago = pagos.find(p => p.id === id);
    if(pago){
      resolve(pago);
    } else {
      reject(`No existe el pago con el id ${id}`);
    }
  });
};

getCliente(3)
  .then(cliente => {
    console.log("PROMESA");
    console.log(cliente)
  })
  .catch(error => {
    console.log("PROMESA");
    console.log(error);
});

getPagoP(3)
  .then(pago => console.log(pago))
  .catch(error => {
    console.log(error);
});

// PROMESA EN CADENA

getCliente(1)
  .then((cliente) => {
    getPagoP(2)
      .then(pago => {
        console.log("PROMESA EN CADENA");
        console.log(`El clente ${cliente.nombre} recibe un pago de ${pago.pago}`)
      })
      .catch(error => console.log(error))
  })
  .catch(error => console.log(error)
  )

//ASYNC-AWAIT

const getInfoClient = async (id) => {
  try {
    const cliente = await getCliente(id);
    const pago = await getPagoP(id);
    console.log("ASYNC-AWAIT");
    return `El pago del cliente: ${cliente.nombre} es de ${pago['pago']}`;
  }
  catch(ex){
    throw ex;
  }
};

getInfoClient(2)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));